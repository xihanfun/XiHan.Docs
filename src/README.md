---
home: true
icon: home
title: 首页
heroImage: /logo.svg
heroText: 曦寒官方文档
tagline: 打造个人知识产权的新型全场景应用软件
actions:
  - text: 项目简介
    link: /guide/
    type: secondary

  - text: 快速上手
    link: /docs/
    type: primary

features:
  - title: 代码开源
    icon: layout
    details: 所有代码开源在 GitHub 和 Gitee 上且处于积极维护状态，您也可以在上面提交您的问题或者参与代码贡献。
    link:

  - title: 强力驱动
    icon: slides
    details: 后端由 .NET7 驱动，前端由 Vue3 驱动，容器化、跨平台、云原生带来了持续的性能改进。
    link:

  - title: 简洁至上
    icon: markdown
    details: 以 Markdown 为中心的项目结构，以最少的配置帮助你自定义站点和专注写作。高效快速 拥抱开源 用心创作 探索未知
    link:

copyright: MIT Licensed | Copyright ©2023  <a href="https://www.zhaifanhua.com" target="_blank">ZhaiFanhua</a> All Rights Reserved.
---
